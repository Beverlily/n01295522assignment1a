﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="servercontrol.aspx.cs" Inherits="RestaurantBooking.servercontrol" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
 <form id="form1" runat="server">
        <div>
          <h1>Beverly's 24 Hour Restaurant</h1>
          <p>Welcome! Make a reservation for today at Beverly's 24 Hour Restaurant.</p>
          <asp:Label ID="NameLabel" AssociatedControlID="Name" runat="server" Text="Name:" Font-Bold="true"></asp:Label>
          <asp:TextBox ID="Name" runat="server" placeholder="Name"></asp:TextBox>
          <asp:RequiredFieldValidator ID="NameValidator" runat="server"  ControlToValidate="Name" ErrorMessage="Please enter a name"></asp:RequiredFieldValidator>
          <asp:RegularExpressionValidator ID="NameRegex" runat="server" ValidationExpression="^[a-zA-Z- ]+$" ControlToValidate="Name" ErrorMessage="Name must contain only letters"></asp:RegularExpressionValidator>
          <br />

          <asp:Label ID="EmailLabel" AssociatedControlID="Email" runat="server" Text="Email:" Font-Bold="true"></asp:Label>
          <asp:TextBox ID="Email" runat="server" placeholder="Email"></asp:TextBox>
          <asp:RequiredFieldValidator ID="EmailValidator" runat="server"  ControlToValidate="Email" ErrorMessage="Please enter an email address"></asp:RequiredFieldValidator>
          <asp:RegularExpressionValidator ID="EmailRegex" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="Email" ErrorMessage="Invalid email format"></asp:RegularExpressionValidator>
          <br />

          <asp:Label ID="PhoneLabel" AssociatedControlID="Phone" runat="server" Text="Phone Number:" Font-Bold="true"></asp:Label>
          <asp:TextBox ID="Phone" runat="server" placeholder="Phone Number"></asp:TextBox>
          <asp:RequiredFieldValidator ID="PhoneValidator" runat="server"  ControlToValidate="Phone" ErrorMessage="Please enter a phone number"></asp:RequiredFieldValidator>
          <asp:RegularExpressionValidator ID="PhoneRegex" runat="server" ValidationExpression="\d{10}" ControlToValidate="Phone" ErrorMessage="Invalid phone format, must be 10 digits"></asp:RegularExpressionValidator>
          <br />

          <asp:Label ID="TimeLabel" AssociatedControlID="Time" runat="server" Text="Reservation Time:" Font-Bold="true"></asp:Label>
          <asp:TextBox ID="Time" runat="server" TextMode="Time"></asp:TextBox>
          <asp:RequiredFieldValidator ID="TimeValidator" runat="server"  ControlToValidate="Time" ErrorMessage="Please select a reservation time"></asp:RequiredFieldValidator>
          <br />

          <asp:Label ID="GuestNumberLabel" AssociatedControlID="GuestNumber" runat="server" Text="Number of Guests:" Font-Bold="true"></asp:Label>
          <asp:TextBox ID="GuestNumber" runat="server" placeholder="Number of Guests"></asp:TextBox>
          <asp:RequiredFieldValidator ID="GuestValidator" runat="server" ControlToValidate="GuestNumber" ErrorMessage="Please enter a guest number"></asp:RequiredFieldValidator>
          <asp:RangeValidator ID="GuestRange" runat="server" ControlToValidate="GuestNumber" MinimumValue="1" MaximumValue="10" Type="Integer" ErrorMessage="Guest number must be between 1 and 10"></asp:RangeValidator>
          <br />

          <asp:Label ID="LocationLabel" AssociatedControlID="LocationList" runat="server" Text="Location:" Font-Bold="true"></asp:Label>
          <asp:DropDownList ID="LocationList" runat="server" DataTextField="Location">
            <asp:ListItem Value="Richmond Hill" Text="Richmond Hill"></asp:ListItem>
            <asp:ListItem Value="Markham" Text="Markham"></asp:ListItem>
            <asp:ListItem Value="Toronto" Text="Toronto"></asp:ListItem>
          </asp:DropDownList>
          <asp:RequiredFieldValidator ID="LocationValidator" runat="server" ControlToValidate="LocationList" ErrorMessage="Please select a Location"></asp:RequiredFieldValidator>
          <br />

          <asp:Label ID="ConfirmationLabel"  AssociatedControlID="options" runat="server" Text="Send Reservation Confirmation Message To:" Font-Bold="true"></asp:Label>
          <div id="options" runat="server">
           <asp:CheckBox ID="MessagePhone" runat="server" Text="Phone via Text" />
           <asp:CheckBox ID="CallPhone" runat="server" Text="Phone via Call" />
           <asp:CheckBox ID="MessageEmail" runat="server" Text="Email" />
          </div>
          <br />

          <asp:Label id="PromotionLabel" AssociatedControlID="Promotion" runat="server" Text="Would you like to signup to receive emails/texts about future promotions for our store?" Font-Bold="true"></asp:Label>
          <asp:RadioButtonList ID="Promotion" runat="server" >
            <asp:ListItem Text="Yes" Value="Yes" />
            <asp:ListItem Text="No" Value="No" />
          </asp:RadioButtonList>
          <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Promotion" ErrorMessage="Please select whether or not you would like to receieve information about our promotions"></asp:RequiredFieldValidator>
          
          <br />
             <asp:Button runat="server" ID="myButton" OnClick="MakeReservation" Text="Submit"/>
          <br />
           <asp:ValidationSummary 
            ID="ValidationSum"
            DisplayMode= "BulletList"
            runat="server"
            HeaderText="The following errors have occurred:"
            ShowMessageBox="true"
            ShowSummary="false"
            />

       </div>
    </form>
</body>
</html>

   