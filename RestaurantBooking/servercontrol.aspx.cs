﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RestaurantBooking
{
    public partial class servercontrol : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void MakeReservation(object sender, EventArgs e)
        {
            List<string> confirmationOptions = new List<string>();

            foreach (Control control in options.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox option = (CheckBox)control;
                    if (option.Checked)
                    {
                        confirmationOptions.Add(option.Text);
                    }

                }
            }

            Client client = new Client(Name.Text, Phone.Text, Email.Text, Promotion.Text);
            Reservation guestReservation = new Reservation(Time.Text, GuestNumber.Text, LocationList.Text, confirmationOptions);
        }
    }
}


