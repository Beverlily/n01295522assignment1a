﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestaurantBooking
{
    public class Reservation
    {
        public string time;
        public string guestNum;
        public string location;
        public List<string> sendConfirmationTo;

        public Reservation()
        {
        }

        public Reservation(string time, string guestNum, string location, List<string> sendConfirmationTo)
        {
            this.time = time;
            this.guestNum = guestNum;
            this.location = location;
            this.sendConfirmationTo = sendConfirmationTo;
        }

    }
}
