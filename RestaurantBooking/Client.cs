﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestaurantBooking
{

    public class Client
    {
        private string name;
        private string email;
        private string phone;
        private string promotion;
        public List<string> confirmationOptions;

        public Client(string name, string email, string phone, string promotion)
        {
            this.name = name;
            this.email = email;
            this.phone = phone;
            this.promotion = promotion;
        }

        public string ClientName
        {
            get { return name; }
            set { name = value; }
        }

        public string ClientEmail
        {
            get { return email; }
            set { email = value; }
        }

        public string ClientPhone
        {
            get { return phone; }
            set { phone = value; }
        }

        public string ClientPromotion
        {
            get { return promotion; }
            set { promotion = value; }
        }

    } 

}